﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 13.11.2021
 * Time: 15:34
 */
using System;
using System.Collections.Generic;

namespace RandomCheck
{
	/// <summary>
	/// Линейный конгруэнтный генератор случайных чисел.
	/// </summary>
	public class CustomRandom
	{
		public static Type[] ArgTypes(bool UseSeed) {
			var ti = Type.GetType("Int32");
			return UseSeed ? new Type[] { ti, ti, ti, ti } : new Type[] { ti, ti, ti };
		}
		
		public static object[] ArgValues(IConvertible _a, IConvertible _m, IConvertible _c, IConvertible _seed = null) {
			var ol = new List<object>();
			ol.Add(Convert.ToInt32(_a));
			ol.Add(Convert.ToInt32(_m));
			ol.Add(Convert.ToInt32(_c));
			if (_seed != null) ol.Add(Convert.ToInt32(_seed));
			return ol.ToArray();
		}
		
		protected readonly int min = 0;
		protected readonly int max = int.MaxValue;
		protected int a, m, c;
		protected int seed;
		
		int value;
		
		public CustomRandom(int _a, int _m, int _c, int _seed) {
			a = _a; max = m = _m; c = _c; value = seed = _seed;
		}
		
		public CustomRandom(int _a, int _m, int _c) : this(_a, _m, _c, Environment.TickCount) {}
		
		public int Next() {
			long result;
			Math.DivRem(Math.BigMul(a, value) + c, (long)m, out result);
			value = (int)result;
			return value;
		}
		
		public double NextDouble() {
			return (double)(Next() - min) / (double)(max - min);
		}
	}
}

﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 13.11.2021
 * Time: 13:24
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace RandomCheck
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		enum ProcMode { Time, Period, Uniformity, Correlation }
		ProcMode Mode;
		
		//readonly Type InternalRandomType = Type.GetType("Random");
		//readonly Type CustomRandomType = Type.GetType("CustomRandom");
		
		public MainForm()
		{
			InitializeComponent();
			Bn_TestPeriod.Tag = ProcMode.Period;
			Bn_TestUniform.Tag = ProcMode.Uniformity;
			Bn_TestCorrelation.Tag = ProcMode.Correlation;
			Sel_UseInternal.Checked = true;
			CB_Chi2Level.SelectedIndex = 5;
		}
		
		void GeneratorCheckedChanged(object sender, EventArgs e)
		{
			NUD_GenPeriod.Enabled = NUD_GenMulti.Enabled = NUD_GenBias.Enabled = Sel_UseCustom.Checked;
			tableLayoutPanel1.RowStyles[1].Height = Sel_UseInternal.Checked ? 44 : 66;
		}
		
		void UseSeedCheckedChanged(object sender, EventArgs e)
		{
			NUD_Seed.Enabled = Check_UseSeed.Checked;
		}
		
		void ProcMainDoWork(object sender, DoWorkEventArgs e)
		{
			//object rand;
			//if (Sel_UseInternal.Checked) 
			//	rand = InternalRandomType.GetConstructor(Check_UseSeed.Checked ? new Type[] { Type.GetType("Int32") } : Type.EmptyTypes)
			//		.Invoke(Check_UseSeed.Checked ? new object[] { Convert.ToInt32(NUD_Seed.Value) } : new object[] {} );
			//	else rand = CustomRandomType.GetConstructor(CustomRandom.ArgTypes(Check_UseSeed.Checked))
			//		.Invoke(CustomRandom.ArgValues(NUD_GenPeriod.Value, NUD_GenMulti.Value, NUD_GenBias.Value, Check_UseSeed.Checked ? NUD_Seed.Value : null));
			//	MethodInfo randNext = (Sel_UseInternal.Checked ? InternalRandomType : CustomRandomType).GetMethod("Next");
			//	MethodInfo randNextDouble = (Sel_UseInternal.Checked ? InternalRandomType : CustomRandomType).GetMethod("NextDouble");
			
			object rand;
			Func<int> randNext;
			Func<double> randNextDouble;
			int max;
			if (Sel_UseInternal.Checked) {
				rand = Check_UseSeed.Checked ? new Random(Convert.ToInt32(NUD_Seed.Value)) : new Random();
				randNext = (rand as Random).Next;
				randNextDouble = (rand as Random).NextDouble;
				max = int.MaxValue;
			}
			else {
				int m = Convert.ToInt32(NUD_GenPeriod.Value);
				int a = Convert.ToInt32(NUD_GenMulti.Value);
				int c = Convert.ToInt32(NUD_GenBias.Value);
				rand = Check_UseSeed.Checked ? new CustomRandom(a, m, c, Convert.ToInt32(NUD_Seed.Value)) : new CustomRandom(a, m, c);
				randNext = (rand as CustomRandom).Next;
				randNextDouble = (rand as CustomRandom).NextDouble;
				max = m;
			}
			
			int N = Convert.ToInt32(NUD_TestsCount.Value);
			var worker = sender as BackgroundWorker;
			switch (Mode) {
				case ProcMode.Time:
					break;
					
				case ProcMode.Period:
					int z = 0, q = 0;
					worker.ReportProgress(0, z);
					
					var periodmark = new int[32];
					var periodpos = new List<int>();
					for (int i = 0; i < 96; i++) {
						if (randNextDouble() > 0.5) z++;
						else z--;
					}
					periodpos.Add(96 + 32);
					for (int i = 0; i < 32; i++) {
						if (randNextDouble() > 0.5) z++;
						else z--;
						periodmark[i] = z;
					}
					
					for (int i = 0; i < N; i++) {
						if (worker.CancellationPending) { e.Cancel = true; break; }
						
						//double value = (double)randNextDouble.Invoke(rand, new object[] {});
						double value = randNextDouble();
						if (value > 0.5)
							z++;
						else z--;
						
						if (z == periodmark[q]) q++;
						else q = 0;
						if (q >= periodmark.Length) {
							periodpos.Add(i);
							q = 0;
						}
						
						if (i % (N / 1000) == 0) {
							worker.ReportProgress(i + 1, z);
							System.Threading.Thread.Sleep(1);
						}
						i++;
					}
					e.Result = periodpos;
					break;
					
				case ProcMode.Uniformity:
					int M = Convert.ToInt32(NUD_SamplesCount.Value);
					var basket = new int[M];
					int step = max / M;
					for (int i = 0; i < N; i++) {
						if (worker.CancellationPending) { e.Cancel = true; break; }
						
						//int value = (int)randNext.Invoke(rand, new object[] {});
						int value = randNext();
						for (int k = 0; k < M; k++) {
							if (value >= k * step && value < (k + 1) * step) {
								basket[k]++;
								break;
							}
						}
						
						if (i % (N / 1000) == 0) {
							worker.ReportProgress(i, basket);
							System.Threading.Thread.Sleep(1);
						}
					}
					
					double mean = (double)N / (double)M;
					double sum = 0;
					for (int k = 0; k < M; k++)
						sum += (basket[k] - mean) * (basket[k] - mean) / mean;
					e.Result = sum;
					break;
					
				case ProcMode.Correlation:
					var casket = new double[N];
					int K = Convert.ToInt32(NUD_CorrelationLength.Value);
					for (int k = 1; k <= K; k++) {
						double cov = 0, s0 = 0, sk = 0;
						// double mean_0 = 0, mean_k = 0, mean_mult = 0;
						for (int i = 0; i < N; i++) {
							if (worker.CancellationPending) { e.Cancel = true; break; }
							
							//casket[i] = (int)randNextDouble.Invoke(rand, new object[] {});
							casket[i] = randNextDouble();
							// mean_0 += casket[i];
							s0 += (casket[i] - 0.5) * (casket[i] - 0.5);
							
							if (i >= k) {
								// mean_k += casket[i];
								// mean_mult += casket[i] * casket[i - k];
								sk += (casket[i] - 0.5) * (casket[i] - 0.5);
								cov += (casket[i] - 0.5) * (casket[i - k] - 0.5);
							}
						}
						
						double result = cov / Math.Sqrt(s0 * sk);
						worker.ReportProgress(k, new double[] { result, s0, sk, cov });
						
						// mean_0 /= N;
						// mean_k /= (double)(N - k);
						// mean_mult /= N * (double)(N - k);
						// double result = (mean_mult - mean_0 * mean_k) / (mean_k * mean_k - mean_0 * mean_0);
						// worker.ReportProgress(k, new double[] { result, mean_0, mean_k, mean_mult });
					}
					break;
					
				default:
					throw new ArgumentException();
			}
		}
		
		void ProcMainProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			switch (Mode) {
				case ProcMode.Time:
					break;
				case ProcMode.Period:
					ChartMain.Series[0].Points.AddXY(e.ProgressPercentage, (int)e.UserState);
					break;
				case ProcMode.Uniformity:
					var basket = e.UserState as int[];
					ChartMain.Series[0].Points.Clear();
					for (int i = 0; i < basket.Length; i++)
						ChartMain.Series[0].Points.AddXY(i, basket[i]);
					break;
				case ProcMode.Correlation:
					var debugvalues = e.UserState as double[];
					ChartMain.Series[0].Points.AddXY(e.ProgressPercentage, debugvalues[0]);
					break;
				default:
					throw new ArgumentException();
			}
		}
		
		void ProcMainRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			switch (Mode) {
				case ProcMode.Time:
					break;
				case ProcMode.Period:
					if ((e.Result as IList<int>).Count > 1) {
						var pos = e.Result as IList<int>;
						int period = 0;
						for (int i = 1; i < pos.Count; i++)
							period += pos[i] - pos[i - 1];
						MessageBox.Show(String.Format("Средний период: {0:F0}", period / (double)(pos.Count - 1)));
					}
					else MessageBox.Show("Периодичность не обнаружена!");
					break;
				case ProcMode.Uniformity:
					MessageBox.Show(String.Format("Наблюдаемый критерий: {0:F3}\nЗначение хи-квадрат: {1:F3}", (double)e.Result, ChiSquared.GetValue(CB_Chi2Level.SelectedIndex, Convert.ToInt32(NUD_SamplesCount.Value))));
					break;
				case ProcMode.Correlation:
					break;
				default:
					throw new ArgumentException();
			}
			Enabled = true;
		}
		
		void BnTestClick(object sender, EventArgs e)
		{
			Enabled = false;
			Mode = (ProcMode)(sender as Control).Tag;
			
			ChartMain.Series.Clear();
			var serie = new Series { ChartArea = "Default" };
			switch (Mode) {
				case ProcMode.Time:
					break;
				case ProcMode.Period:
					serie.ChartType = SeriesChartType.Line;
					break;
				case ProcMode.Uniformity:
					serie.ChartType = SeriesChartType.Column;
					break;
				case ProcMode.Correlation:
					serie.ChartType = SeriesChartType.Point;
					break;
				default:
					throw new ArgumentException();
			}
			ChartMain.Series.Add(serie);
			
			ProcMain.RunWorkerAsync();
		}
	}
}

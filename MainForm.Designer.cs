﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 13.11.2021
 * Time: 13:24
 */
namespace RandomCheck
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.DataVisualization.Charting.Chart ChartMain;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown NUD_TestsCount;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.RadioButton Sel_UseCustom;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown NUD_GenPeriod;
		private System.Windows.Forms.RadioButton Sel_UseInternal;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Button Bn_TestCorrelation;
		private System.Windows.Forms.Button Bn_TestUniform;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown NUD_CorrelationLength;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown NUD_SamplesCount;
		private System.Windows.Forms.Button Bn_TestPeriod;
		private System.Windows.Forms.NumericUpDown NUD_GenBias;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown NUD_GenMulti;
		private System.Windows.Forms.NumericUpDown NUD_Seed;
		private System.Windows.Forms.CheckBox Check_UseSeed;
		private System.ComponentModel.BackgroundWorker ProcMain;
		private System.Windows.Forms.ComboBox CB_Chi2Level;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.NUD_GenBias = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.NUD_GenMulti = new System.Windows.Forms.NumericUpDown();
			this.Sel_UseCustom = new System.Windows.Forms.RadioButton();
			this.label8 = new System.Windows.Forms.Label();
			this.NUD_GenPeriod = new System.Windows.Forms.NumericUpDown();
			this.Sel_UseInternal = new System.Windows.Forms.RadioButton();
			this.ChartMain = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.NUD_Seed = new System.Windows.Forms.NumericUpDown();
			this.Bn_TestCorrelation = new System.Windows.Forms.Button();
			this.Bn_TestUniform = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.NUD_CorrelationLength = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.NUD_SamplesCount = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.NUD_TestsCount = new System.Windows.Forms.NumericUpDown();
			this.Bn_TestPeriod = new System.Windows.Forms.Button();
			this.Check_UseSeed = new System.Windows.Forms.CheckBox();
			this.CB_Chi2Level = new System.Windows.Forms.ComboBox();
			this.ProcMain = new System.ComponentModel.BackgroundWorker();
			this.tableLayoutPanel1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.NUD_GenBias)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_GenMulti)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_GenPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ChartMain)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.NUD_Seed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_CorrelationLength)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_SamplesCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_TestsCount)).BeginInit();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.ChartMain, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 4);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 84F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(512, 490);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.tableLayoutPanel3);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(0, 84);
			this.groupBox2.Margin = new System.Windows.Forms.Padding(0);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox2.Size = new System.Drawing.Size(512, 66);
			this.groupBox2.TabIndex = 4;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Параметры генератора";
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 7;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Controls.Add(this.NUD_GenBias, 5, 2);
			this.tableLayoutPanel3.Controls.Add(this.label5, 4, 2);
			this.tableLayoutPanel3.Controls.Add(this.label3, 2, 2);
			this.tableLayoutPanel3.Controls.Add(this.NUD_GenMulti, 3, 2);
			this.tableLayoutPanel3.Controls.Add(this.Sel_UseCustom, 2, 0);
			this.tableLayoutPanel3.Controls.Add(this.label8, 0, 2);
			this.tableLayoutPanel3.Controls.Add(this.NUD_GenPeriod, 1, 2);
			this.tableLayoutPanel3.Controls.Add(this.Sel_UseInternal, 0, 0);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 17);
			this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 4;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(504, 45);
			this.tableLayoutPanel3.TabIndex = 0;
			// 
			// NUD_GenBias
			// 
			this.NUD_GenBias.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NUD_GenBias.Increment = new decimal(new int[] {
			128,
			0,
			0,
			0});
			this.NUD_GenBias.Location = new System.Drawing.Point(416, 24);
			this.NUD_GenBias.Margin = new System.Windows.Forms.Padding(0);
			this.NUD_GenBias.Maximum = new decimal(new int[] {
			65535,
			0,
			0,
			0});
			this.NUD_GenBias.Name = "NUD_GenBias";
			this.NUD_GenBias.Size = new System.Drawing.Size(64, 20);
			this.NUD_GenBias.TabIndex = 7;
			// 
			// label5
			// 
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Location = new System.Drawing.Point(320, 24);
			this.label5.Margin = new System.Windows.Forms.Padding(0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(96, 20);
			this.label5.TabIndex = 6;
			this.label5.Text = "Смещение (c)";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(160, 24);
			this.label3.Margin = new System.Windows.Forms.Padding(0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(96, 20);
			this.label3.TabIndex = 4;
			this.label3.Text = "Множитель (a)";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// NUD_GenMulti
			// 
			this.NUD_GenMulti.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NUD_GenMulti.Increment = new decimal(new int[] {
			128,
			0,
			0,
			0});
			this.NUD_GenMulti.Location = new System.Drawing.Point(256, 24);
			this.NUD_GenMulti.Margin = new System.Windows.Forms.Padding(0);
			this.NUD_GenMulti.Maximum = new decimal(new int[] {
			2147483647,
			0,
			0,
			0});
			this.NUD_GenMulti.Name = "NUD_GenMulti";
			this.NUD_GenMulti.Size = new System.Drawing.Size(64, 20);
			this.NUD_GenMulti.TabIndex = 5;
			this.NUD_GenMulti.Value = new decimal(new int[] {
			899,
			0,
			0,
			0});
			// 
			// Sel_UseCustom
			// 
			this.tableLayoutPanel3.SetColumnSpan(this.Sel_UseCustom, 3);
			this.Sel_UseCustom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Sel_UseCustom.Location = new System.Drawing.Point(256, 0);
			this.Sel_UseCustom.Margin = new System.Windows.Forms.Padding(0);
			this.Sel_UseCustom.Name = "Sel_UseCustom";
			this.Sel_UseCustom.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
			this.Sel_UseCustom.Size = new System.Drawing.Size(224, 20);
			this.Sel_UseCustom.TabIndex = 3;
			this.Sel_UseCustom.Text = "Настраиваемый генератор";
			this.Sel_UseCustom.UseVisualStyleBackColor = true;
			this.Sel_UseCustom.CheckedChanged += new System.EventHandler(this.GeneratorCheckedChanged);
			// 
			// label8
			// 
			this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label8.Location = new System.Drawing.Point(0, 24);
			this.label8.Margin = new System.Windows.Forms.Padding(0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(96, 20);
			this.label8.TabIndex = 0;
			this.label8.Text = "Период (m)";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// NUD_GenPeriod
			// 
			this.NUD_GenPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NUD_GenPeriod.Increment = new decimal(new int[] {
			128,
			0,
			0,
			0});
			this.NUD_GenPeriod.Location = new System.Drawing.Point(96, 24);
			this.NUD_GenPeriod.Margin = new System.Windows.Forms.Padding(0);
			this.NUD_GenPeriod.Maximum = new decimal(new int[] {
			2147483647,
			0,
			0,
			0});
			this.NUD_GenPeriod.Name = "NUD_GenPeriod";
			this.NUD_GenPeriod.Size = new System.Drawing.Size(64, 20);
			this.NUD_GenPeriod.TabIndex = 1;
			this.NUD_GenPeriod.Value = new decimal(new int[] {
			32768,
			0,
			0,
			0});
			// 
			// Sel_UseInternal
			// 
			this.tableLayoutPanel3.SetColumnSpan(this.Sel_UseInternal, 3);
			this.Sel_UseInternal.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Sel_UseInternal.Location = new System.Drawing.Point(0, 0);
			this.Sel_UseInternal.Margin = new System.Windows.Forms.Padding(0);
			this.Sel_UseInternal.Name = "Sel_UseInternal";
			this.Sel_UseInternal.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
			this.Sel_UseInternal.Size = new System.Drawing.Size(256, 20);
			this.Sel_UseInternal.TabIndex = 2;
			this.Sel_UseInternal.Text = "Встроенный генератор";
			this.Sel_UseInternal.UseVisualStyleBackColor = true;
			this.Sel_UseInternal.CheckedChanged += new System.EventHandler(this.GeneratorCheckedChanged);
			// 
			// ChartMain
			// 
			chartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
			chartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
			chartArea5.Name = "Default";
			this.ChartMain.ChartAreas.Add(chartArea5);
			this.ChartMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ChartMain.Location = new System.Drawing.Point(0, 150);
			this.ChartMain.Margin = new System.Windows.Forms.Padding(0);
			this.ChartMain.Name = "ChartMain";
			this.ChartMain.Size = new System.Drawing.Size(512, 340);
			this.ChartMain.TabIndex = 3;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tableLayoutPanel2);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox1.Size = new System.Drawing.Size(512, 84);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Параметры тестирования";
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 7;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 88F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Controls.Add(this.NUD_Seed, 1, 2);
			this.tableLayoutPanel2.Controls.Add(this.Bn_TestCorrelation, 4, 0);
			this.tableLayoutPanel2.Controls.Add(this.Bn_TestUniform, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.label6, 4, 1);
			this.tableLayoutPanel2.Controls.Add(this.NUD_CorrelationLength, 5, 1);
			this.tableLayoutPanel2.Controls.Add(this.label4, 2, 2);
			this.tableLayoutPanel2.Controls.Add(this.label2, 2, 1);
			this.tableLayoutPanel2.Controls.Add(this.NUD_SamplesCount, 3, 1);
			this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.NUD_TestsCount, 1, 1);
			this.tableLayoutPanel2.Controls.Add(this.Bn_TestPeriod, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.Check_UseSeed, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.CB_Chi2Level, 3, 2);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 17);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 4;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(504, 63);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// NUD_Seed
			// 
			this.NUD_Seed.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NUD_Seed.Enabled = false;
			this.NUD_Seed.Increment = new decimal(new int[] {
			128,
			0,
			0,
			0});
			this.NUD_Seed.Location = new System.Drawing.Point(80, 42);
			this.NUD_Seed.Margin = new System.Windows.Forms.Padding(0);
			this.NUD_Seed.Maximum = new decimal(new int[] {
			32767,
			0,
			0,
			0});
			this.NUD_Seed.Name = "NUD_Seed";
			this.NUD_Seed.Size = new System.Drawing.Size(88, 20);
			this.NUD_Seed.TabIndex = 14;
			// 
			// Bn_TestCorrelation
			// 
			this.tableLayoutPanel2.SetColumnSpan(this.Bn_TestCorrelation, 2);
			this.Bn_TestCorrelation.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Bn_TestCorrelation.Location = new System.Drawing.Point(336, 0);
			this.Bn_TestCorrelation.Margin = new System.Windows.Forms.Padding(0);
			this.Bn_TestCorrelation.Name = "Bn_TestCorrelation";
			this.Bn_TestCorrelation.Size = new System.Drawing.Size(168, 22);
			this.Bn_TestCorrelation.TabIndex = 12;
			this.Bn_TestCorrelation.Text = "Проверка корреляции";
			this.Bn_TestCorrelation.UseVisualStyleBackColor = true;
			this.Bn_TestCorrelation.Click += new System.EventHandler(this.BnTestClick);
			// 
			// Bn_TestUniform
			// 
			this.tableLayoutPanel2.SetColumnSpan(this.Bn_TestUniform, 2);
			this.Bn_TestUniform.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Bn_TestUniform.Location = new System.Drawing.Point(168, 0);
			this.Bn_TestUniform.Margin = new System.Windows.Forms.Padding(0);
			this.Bn_TestUniform.Name = "Bn_TestUniform";
			this.Bn_TestUniform.Size = new System.Drawing.Size(168, 22);
			this.Bn_TestUniform.TabIndex = 11;
			this.Bn_TestUniform.Text = "Проверка равномерности";
			this.Bn_TestUniform.UseVisualStyleBackColor = true;
			this.Bn_TestUniform.Click += new System.EventHandler(this.BnTestClick);
			// 
			// label6
			// 
			this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label6.Location = new System.Drawing.Point(336, 22);
			this.label6.Margin = new System.Windows.Forms.Padding(0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(120, 20);
			this.label6.TabIndex = 8;
			this.label6.Text = "Период корреляции";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// NUD_CorrelationLength
			// 
			this.NUD_CorrelationLength.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NUD_CorrelationLength.Location = new System.Drawing.Point(456, 22);
			this.NUD_CorrelationLength.Margin = new System.Windows.Forms.Padding(0);
			this.NUD_CorrelationLength.Maximum = new decimal(new int[] {
			10,
			0,
			0,
			0});
			this.NUD_CorrelationLength.Minimum = new decimal(new int[] {
			1,
			0,
			0,
			0});
			this.NUD_CorrelationLength.Name = "NUD_CorrelationLength";
			this.NUD_CorrelationLength.Size = new System.Drawing.Size(48, 20);
			this.NUD_CorrelationLength.TabIndex = 9;
			this.NUD_CorrelationLength.Value = new decimal(new int[] {
			5,
			0,
			0,
			0});
			// 
			// label4
			// 
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Location = new System.Drawing.Point(168, 42);
			this.label4.Margin = new System.Windows.Forms.Padding(0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(120, 20);
			this.label4.TabIndex = 5;
			this.label4.Text = "Ур. значимости, %";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(168, 22);
			this.label2.Margin = new System.Windows.Forms.Padding(0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(120, 20);
			this.label2.TabIndex = 2;
			this.label2.Text = "Кол-во отсчётов";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// NUD_SamplesCount
			// 
			this.NUD_SamplesCount.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NUD_SamplesCount.Location = new System.Drawing.Point(288, 22);
			this.NUD_SamplesCount.Margin = new System.Windows.Forms.Padding(0);
			this.NUD_SamplesCount.Minimum = new decimal(new int[] {
			1,
			0,
			0,
			0});
			this.NUD_SamplesCount.Name = "NUD_SamplesCount";
			this.NUD_SamplesCount.Size = new System.Drawing.Size(48, 20);
			this.NUD_SamplesCount.TabIndex = 3;
			this.NUD_SamplesCount.Value = new decimal(new int[] {
			25,
			0,
			0,
			0});
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(0, 22);
			this.label1.Margin = new System.Windows.Forms.Padding(0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Кол-во тестов";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// NUD_TestsCount
			// 
			this.NUD_TestsCount.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NUD_TestsCount.Increment = new decimal(new int[] {
			100,
			0,
			0,
			0});
			this.NUD_TestsCount.Location = new System.Drawing.Point(80, 22);
			this.NUD_TestsCount.Margin = new System.Windows.Forms.Padding(0);
			this.NUD_TestsCount.Maximum = new decimal(new int[] {
			0,
			232,
			0,
			0});
			this.NUD_TestsCount.Name = "NUD_TestsCount";
			this.NUD_TestsCount.Size = new System.Drawing.Size(88, 20);
			this.NUD_TestsCount.TabIndex = 1;
			this.NUD_TestsCount.Value = new decimal(new int[] {
			1000000,
			0,
			0,
			0});
			// 
			// Bn_TestPeriod
			// 
			this.tableLayoutPanel2.SetColumnSpan(this.Bn_TestPeriod, 2);
			this.Bn_TestPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Bn_TestPeriod.Location = new System.Drawing.Point(0, 0);
			this.Bn_TestPeriod.Margin = new System.Windows.Forms.Padding(0);
			this.Bn_TestPeriod.Name = "Bn_TestPeriod";
			this.Bn_TestPeriod.Size = new System.Drawing.Size(168, 22);
			this.Bn_TestPeriod.TabIndex = 10;
			this.Bn_TestPeriod.Text = "Определить период";
			this.Bn_TestPeriod.UseVisualStyleBackColor = true;
			this.Bn_TestPeriod.Click += new System.EventHandler(this.BnTestClick);
			// 
			// Check_UseSeed
			// 
			this.Check_UseSeed.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.Check_UseSeed.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Check_UseSeed.Location = new System.Drawing.Point(0, 42);
			this.Check_UseSeed.Margin = new System.Windows.Forms.Padding(0);
			this.Check_UseSeed.Name = "Check_UseSeed";
			this.Check_UseSeed.Padding = new System.Windows.Forms.Padding(0, 0, 4, 0);
			this.Check_UseSeed.Size = new System.Drawing.Size(80, 20);
			this.Check_UseSeed.TabIndex = 13;
			this.Check_UseSeed.Text = "Затравка";
			this.Check_UseSeed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.Check_UseSeed.UseVisualStyleBackColor = true;
			this.Check_UseSeed.CheckedChanged += new System.EventHandler(this.UseSeedCheckedChanged);
			// 
			// CB_Chi2Level
			// 
			this.CB_Chi2Level.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.CB_Chi2Level.FormattingEnabled = true;
			this.CB_Chi2Level.Items.AddRange(new object[] {
			"20",
			"10",
			"5",
			"2.5",
			"2",
			"1",
			"0.5",
			"0.2",
			"0.1"});
			this.CB_Chi2Level.Location = new System.Drawing.Point(288, 42);
			this.CB_Chi2Level.Margin = new System.Windows.Forms.Padding(0);
			this.CB_Chi2Level.Name = "CB_Chi2Level";
			this.CB_Chi2Level.Size = new System.Drawing.Size(48, 21);
			this.CB_Chi2Level.TabIndex = 15;
			// 
			// ProcMain
			// 
			this.ProcMain.WorkerReportsProgress = true;
			this.ProcMain.WorkerSupportsCancellation = true;
			this.ProcMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ProcMainDoWork);
			this.ProcMain.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.ProcMainProgressChanged);
			this.ProcMain.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ProcMainRunWorkerCompleted);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(520, 498);
			this.Controls.Add(this.tableLayoutPanel1);
			this.MinimumSize = new System.Drawing.Size(536, 320);
			this.Name = "MainForm";
			this.Padding = new System.Windows.Forms.Padding(4);
			this.Text = "Проверка генератора случайных чисел";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.tableLayoutPanel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.NUD_GenBias)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_GenMulti)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_GenPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ChartMain)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.NUD_Seed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_CorrelationLength)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_SamplesCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.NUD_TestsCount)).EndInit();
			this.ResumeLayout(false);

		}
	}
}

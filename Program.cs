﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 13.11.2021
 * Time: 13:24
 */
using System;
using System.Windows.Forms;

namespace RandomCheck
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}
		
	}
}
